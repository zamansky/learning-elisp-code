
;; rot13
;; abc def (rotate by 3) z -> c
;; 

(defun rotate (c offset base)
  "Rotate character c by offset"
  (+ (mod (+ (- c base) offset) 26) base))

(defun rotate-13 (c)
  (rotate c 13))

;; (defun rot13 (s)
;;  (concat (mapcar #'rotate-13 s)))


;; Code for the conditionals video (rot13 part 2)
;;
;; (defun rot13 (s)
;;   (concat (mapcar (lambda (c)
;; 		    (if (and (>= c ?a) (<= c ?z))
;; 			(rotate c 13)
;; 		      c))
;; 		  s)))

(defun rot (s offset)
  (concat (mapcar (lambda (c)
		    (cond ((and (>= c ?a) (<= c ?z)) (rotate c offset 97) )
			  ((and (>= c ?A) (<= c ?Z)) (rotate c offset 65) )
			  (t c)) 
		    )
		  s)))

;; Code for interactive (rot13 part 3)
;;
;; (defun rotate-interactive (n)
;;   (interactive "P")
;;   (let* ((offset (if n n 13))
;; 	 (s (word-at-point))
;; 	 (rotated-text (rot s offset) ))
;;     (backward-word 1)
;;     (kill-word 1)
;;     (insert rotated-text)
;;     ))

(keymap-local-set "C-c z" 'rotate-interactive)
(keymap-local-set "C-c C-z" 'rotate-interactive)

(defun rotate-interactive (n)
  (interactive "P")
  (let ((offset (if n n 13)))
    (if mark-active
	(let* ((s (buffer-substring-no-properties (point) (mark)) )
	       (rotated-text (rot s offset)))
	  (kill-region (point) (mark))
	  (insert rotated-text))
      (let* ((s (word-at-point))
	     (rotated-text (rot s offset) ))
	(backward-word 1)
	(kill-word 1)
	(insert rotated-text))  ;; the old stuff
      )))







  
