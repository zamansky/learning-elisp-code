;; int add_two(int a, int b){


(defun function-header-parse-params (line)
  (string-join (mapcar (lambda (x) (format " * @param %s\n" x)) (string-split line ", "))))
  


(defun function-header ()
  (interactive)
  (save-excursion
  (beginning-of-line)
  (set-mark (point))
  (end-of-line)
  (let* ((regex "\\([a-z0-9]+\\) +\\([a-zA-Z0-9_]+\\)(\\([a-zA-Z0-9_ ,]+\\)")
	 (line (buffer-substring-no-properties (mark) (point)))
	 (index (string-match regex line))
	 (retval (match-string 1 line))
	 (func-name (match-string 2 line))
	 (params (match-string 3 line))
	 (header1 (format "\n/**\n * <p>Description </p>\n * @name %s\n *\n" func-name))
	 (header2 (format "%s *\n" (function-header-parse-params params)))
	 (header3 (format " * @return %s\n *\n */" retval))
	 (header (format "%s%s%s\n" header1 header2 header3)))
    (beginning-of-line)
    (insert header)
    )))

;; (keymap-global-set "M-c" 'function-header)
