;; :bat: --> 🦇

(defcustom emoji-list
      '((":gnu:" . "🐃")
	(":panda:" . "🐼")
	(":koala:" . "🐨")
	(":lion:" . "🦁")
	(":racoon:" . "🦝")
	(":turtle:" . "🐢")
	(":snake:" . "🐍")
	(":whale:" . "🐳")
	(":sheep:" . "🐑")
	(":gorilla:" . "🦍")
	(":elephant:" . "🐘")
	(":dolphin:" . "🐬")
	(":squid:" . "🐙")
	(":ghost:" . "👻")
	(":shark:" . "🦈"))
      "List of emojis that can be replaced")

(defcustom emoji-list-by-insert t "t to insert nil to overlay")




(defun emoji-replace-insert (beg end len)
  (let ((s (buffer-substring beg end)))
    (if (string= s " ")
      (save-excursion
	(backward-char 1)
	(set-mark (point))
	(backward-word 1)
	(backward-char 1)
	(let* ((word (buffer-substring-no-properties (point) (mark)))
	       (emoji (cdr (assoc word emoji-list)))
	       )
	  (if (not emoji)
	      nil
	    (kill-region (point) (mark))
	    (insert-before-markers emoji)))))
))


(defun emoji-replace-overlay (beg end len)
  (let ((s (buffer-substring beg end)))
    (if (string= s " ")
      (save-excursion
	(backward-char 1)
	(set-mark (point))
	(backward-word 1)
	(backward-char 1)
	(let* ((word (buffer-substring-no-properties (point) (mark)))
	       (emoji (cdr (assoc word emoji-list)))
	       )
	  (if (not emoji)
	      nil
	    (put-text-property beg end 'display emoji)))))))

;; (add-hook 'after-change-functions #'emoji-replace-insert nil t)
;; (remove-hook 'after-change-functions #'emoji-replace-insert)

;; (add-hook 'after-change-functions #'emoji-replace-overlay nil t)
;; (remove-hook 'after-change-functions #'emoji-replace-overlay)

(define-minor-mode emoji-replace-mode
  "fill in the docstring later"
  :lighter " ER"
  (let ((func (if emoji-replace-by-insert
		  #'emoji-replace-insert
		#'emoji-replace-overlay)))
  (if emoji-replace-mode
      (add-hook 'after-change-functions func nil t)
    (remove-hook 'after-change-functions func))))


