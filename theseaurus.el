
(defvar key "b2cc8af0-500b-47a0-bdcb-b1ce4206c5f6")
(defvar base-url "https://dictionaryapi.com/api/v3/references/thesaurus/json/")

(switch-to-buffer
 (url-retrieve-synchronously (concat base-url "umpire" "?key=" key)))

(defun get-synonyms (word)
  (let* ((url (concat base-url word "?key=" key))
	 (resp (with-current-buffer (url-retrieve-synchronously url)
		 (goto-char (point-min))
		 (re-search-forward "^$")
		 (delete-region (point) (point-min))
		 (buffer-string)))
	 (json-resp (json-parse-string resp))
	 )
    json-resp))

(get-synonyms "umpire")
